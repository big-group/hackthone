#ifndef _BoardLib_
#define _BoardLib_


#include		"stm32f10x_Reg.h"
#include		"stm32f10x_type.h"
#include		"stm32f10x_Init.h"
#include		"delay.h"   
#include		"DataProces.h"
#include 		"KeyFI.h"
#include		"MotorDrive.h"
#include 		"stdio.h"
#include 		"OLED_I2C.h"
#include 		"STM32_I2C.H"
#include 		"Servo.H"
#include		"WIFIDevice.h"
#include		"ATOrg.h" 


#define LED_OutMode   Port_In_Out(PortA,BIT0,GE_TW_2M)
#define LED_ON        (PAout(0)=1)	
#define LED_OFF       (PAout(0)=0)



#define Btt0_InMode   Port_In_Out(PortC,BIT13,Updown_IN)
#define Btt0_UP       (PCout(13)=1)
#define Btt0_InD      PCin(13)

#define Btt1_InMode   Port_In_Out(PortC,BIT14,Updown_IN)
#define Btt1_UP       (PCout(14)=1)
#define Btt1_InD      PCin(14)

#include "BoardLib.h"



void LedFlashSET(usint Keep,usint CY);
void LedFlash(void);
int BoardInit(void);
void BeatHandle_Key(void);





#endif








