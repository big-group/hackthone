#include	"MotorDrive.h"
#include 	"DataProces.h"


#define  MotorRedRatio			56  //电机减速比
#define  MotorWheelPerimeter	201 //轮子周长，单位mm

#define  MotMagNumber  		    12  //霍尔极对数（转动一圈输出的脉冲数）

void MotorDriveInit(void)
{

/*A,B两个马达*/
PWM_Init(4,1);
PWM_Init(4,2);
PWM_Init(4,3);
PWM_Init(4,4);

TIM4_PSC=1; 	//f=72 72/M/PSC+1 =4Mhz
TIM4_ARR=2000;//PWM最大值2000 ，频率=36Mhz/2000

TIM4_CCR1=0;
TIM4_CCR2=0;
TIM4_CCR3=0;
TIM4_CCR4=0;

EncoderInit(); 	//编码器初始化
}




/*
控制电机方向和PWM值，取值-1000~1000；
*/

void MotA(int val)
{

	if(val>20)
	{
		TIM4_CCR1=0;	TIM4_CCR2=val+1000;  //芯片特性，需要50%的启动值
	}
	else if(val<-20)
	{
		val=-val;
		TIM4_CCR1=val+1000;	TIM4_CCR2=0;
	}
	else
	{
		TIM4_CCR1=0;		TIM4_CCR2=0;
	}
}

/*
控制电机方向和PWM值，取值-1000~1000；
*/
void MotB(int val)
{
	

	if(val>20)
	{
		TIM4_CCR3=val+1000;		TIM4_CCR4=0;  //芯片特性，需要50%的启动值
	}
	else if(val<-20)
	{
		val=-val;
		TIM4_CCR3=0;	TIM4_CCR4=val+1000;
		
		
		
	}
	else
	{
		TIM4_CCR3=0;		TIM4_CCR4=0;
	}
}









/***********************************************************

编码器相关

***********************************************************/
#define HA_IRQBit 3
#define HB_IRQBit 4


void EncoderInit(void)
{
	HallA_InMode;
	HallB_InMode;


	Nvic_Init(2,1,0,EXTI3_IRQChannel);
	EXTI_Config(PortB,HA_IRQBit,1);
	EXTI_Enable(HA_IRQBit,1);
	EXTI_MarkCLR(HA_IRQBit);
	
	Nvic_Init(2,1,1,EXTI4_IRQChannel);
	EXTI_Config(PortB,HB_IRQBit,1);
	EXTI_Enable(HB_IRQBit,1);
	EXTI_MarkCLR(HB_IRQBit);

}




ulong Odometer[2]={0,0};		//里程值（编码器累加）
ulong Odometer_Keep[2]={0,0};	//里程值（编码器累加）

long HallSpeed[2][5]={{0,0,0,0,0},{0,0,0,0,0}}; //霍尔转速
long MotSpeed[2]={0,0};

int HallDir[2]={0,0};

void EncoderA_IRQ(void)
{
	static uchar trun=0;
	long i=0;
	
	EXTI_MarkCLR(HA_IRQBit);
	Odometer[0]++; //里程
	
	trun=!trun;
	if(trun)
	{TimerStart(0);i=TimerStop(1);}
	else
	{TimerStart(1);i=TimerStop(0);}

	FIFO_long(i,&HallSpeed[0][0],5);
	HallDir[0]=(HallA_InD)?(1):(0); //方向保存
	

}


void EncoderB_IRQ(void)
{
	static uchar trun=0;
	long i=0;
	
	EXTI_MarkCLR(HB_IRQBit);
	Odometer[1]++; //里程
	
	trun=!trun;
	if(trun)
	{TimerStart(2);i=TimerStop(3);}
	else
	{TimerStart(3);i=TimerStop(2);}

	FIFO_long(i,&HallSpeed[1][0],5);
	HallDir[1]=(HallB_InD)?(1):(0); //方向保存
}



/*
25m一次，用于电机测速计算
*/

void BeatHandle_Encoder(void)
{
	static uchar Cnt=0;
	long temp=0;
	uchar i=0,k=0;
	
	
	Cnt++;
	for(i=0;i<2;i++)
	{
		bubbling_long(&HallSpeed[i][0],5);  //冒泡
		temp=(HallSpeed[i][1]+HallSpeed[i][2]+HallSpeed[i][3])/3; 
		
		//去最大最小，求平均
		temp=temp*MotMagNumber;
		
		if( i==0 )
		temp=(HallDir[i])?(-temp):(temp);
		else
		temp=(HallDir[i])?(temp):(-temp);
		
		MotSpeed[i]=(long)10000000/temp;  //这里比实际放大了10被，为了保持精度
		

		if(Cnt>=4) // 0.1秒钟之内没转到一0.1圈认为停转
		{
			if( (Odometer[i]-Odometer_Keep[i]) <= (ulong)MotMagNumber/10)
			{
				MotSpeed[i]=0;
				for(k=0;k<5;k++) HallSpeed[i][k]=0;
				
			}
			Odometer_Keep[i]=Odometer[i];
		}
	}
	if(Cnt>=4) Cnt=0;
}



/*
返回比实际放大了10被，为了保持精度
转速返回，单位转*10/秒，注意是电机减速前的转速，
减速后的需要除以减速比：MotorRedRatio
*/

long MotorRPM_A(void)
{
	return MotSpeed[0];
}

long MotorRPM_B(void)
{
	return MotSpeed[1];
}




/*
获取线速度，单位mm/s
*/
int GetLinearVelocity_A(void)
{
	long i=MotorRPM_A();
	i=(i*MotorWheelPerimeter)/(MotorRedRatio*10);
	return i;
}



/*
获取线速度，单位mm/s
*/
int GetLinearVelocity_B(void)
{
	long i=MotorRPM_B();
	i=(i*MotorWheelPerimeter)/(MotorRedRatio*10);
	return i;
}






/*****************************************************************


	激光导航部分，需要PID


*****************************************************************/

//设定左右电机速度，单位：mm/s
void set_walkingmotor_speed(int lSpeed, int rSpeed)
{
	/*
	int SPD1=0;
	int SPD2=0;

	SPD1=(long)lSpeed*MotorRedRatio*10/MotorWheelPerimeter;
	SPD2=(long)rSpeed*MotorRedRatio*10/MotorWheelPerimeter;   //放大10倍
	
	PIDSpeed_SetGoal_A(SPD1);  //PID恒速
	PIDSpeed_SetGoal_B(SPD2);
	*/
}



/*
需要循环调用，比如5ms调用一次，根据轮子尺寸算里程
*/
long MileageA_mm=0;
long MileageB_mm=0;

void GetMileage(void)
{
	MileageA_mm=( Odometer[0]*MotorWheelPerimeter)/(MotorRedRatio*MotMagNumber);
	MileageB_mm=( Odometer[1]*MotorWheelPerimeter)/(MotorRedRatio*MotMagNumber);
	
}


//返回左边电机里程数，单位mm
long walkingmotor_cumulate_ldist_mm(void)
{
	return MileageA_mm;
}

//返回右边电机里程数，单位mm
long walkingmotor_cumulate_rdist_mm(void)
{
	return MileageB_mm;
}
	
	

	

