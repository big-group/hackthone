#ifndef  __MoorDrive__
#define  __MoorDrive__		    

#include   "stm32f10x_Reg.h"
#include   "stm32f10x_type.h"
#include   "stm32f10x_Init.h"
#include   "delay.h" 



#define MotDev_Sleep_OutMode Port_In_Out(PortB,BIT1,GE_TW_2M)
#define MotDev_En        	PBout(1)=1
#define MotDev_Ds        	PBout(1)=0

//---------------------------------------------------------------

#define HallA_InMode   Port_In_Out(PortC,BIT15,Updown_IN)
#define HallA_InD      PCin(15)

#define HallB_InMode   Port_In_Out(PortB,BIT5,Updown_IN)
#define HallB_InD      PBin(5)

//---------------------------------------------------------------

void MotorDriveInit(void);

void MotA(int val);
void MotB(int val);

void EncoderInit(void);
void EncoderA_IRQ(void);
void EncoderB_IRQ(void);
void BeatHandle_Encoder(void);
long MotorRPM_A(void);
long MotorRPM_B(void);


/*
获取线速度，单位mm/s
*/
int GetLinearVelocity_A(void);
int GetLinearVelocity_B(void);
void GetMileage(void);

//返回左边电机里程数，单位mm
long walkingmotor_cumulate_ldist_mm(void);
//返回右边电机里程数，单位mm
long walkingmotor_cumulate_rdist_mm(void);

	
void set_walkingmotor_speed(int lSpeed, int rSpeed);




#endif








